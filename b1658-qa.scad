in = 25.4;
fudge = .1;
function org(m) = -(m/2);

// global measurements 
hole_to_hole = 7 * in;	// how far apart are the tractor cylinder and tractor arm
hole_id = 1 * in;	// a 1 inch pin
hole_r = (hole_id / 2);	// ...has a half inch radius
washer_wall = .25 * in;	// The DOM wall
washer_thickness = .25 * in;	// The 'length' of the DOM reinforcement tubing
//overlap = (3/8) * in;	// how much overlap between the inner and outer plates
overlap = .27*in;
width = 2.0*in;	// outer width of tractor bracket
wall = .250*in;	// material thickness
dom_wall = .250*in;
inner_wall_height = 8.5 * in;
outer_wall_height = 9.5 * in;

module bucket_bracket() {
	depth = 2.5*in;
	height = 11.5 * in;

	holes_width = width + (2 * washer_thickness) + (2 * fudge);

	upper_hole_x = org(width); // left wall
	upper_hole_y = (hole_r);	// offset from back of bucket. But actually 'back of hole from Y origin'
	upper_hole_z = (hole_to_hole/2);
	upper_holes_center = [upper_hole_x-washer_thickness, upper_hole_y, upper_hole_z];
	upper_washer_center_r = [upper_hole_x-wall+fudge, upper_hole_y, upper_hole_z];
	upper_washer_center_l = [-upper_hole_x-wall+washer_thickness+fudge, upper_hole_y, upper_hole_z];

	lower_hole_x = org(width);
	lower_hole_y = (hole_r);
	lower_hole_z = (hole_to_hole/2)*-1;
	lower_holes_center = [lower_hole_x-washer_thickness, lower_hole_y, lower_hole_z];
	lower_washer_center_r = [lower_hole_x-wall+fudge, lower_hole_y, lower_hole_z];
	lower_washer_center_l = [-lower_hole_x-wall+washer_thickness+fudge, lower_hole_y, lower_hole_z];

	bracket_center = [org(width), org(depth), org(height)];	// center of the outer bracket is at the origin
	bracket_inner_center = [org(width)+wall, org(depth)+wall, org(height)-fudge];	// center of the inner bracket is at the origin

	bracket_outer = [width, depth, height];	// size of the outer bracket
	bracket_inner = [width - (2 * wall), depth - wall+fudge, height+(fudge*2)]; // size of the inner bracket.  Y is the same, X is 

	difference() {
		difference() {	// channel
			union() {
				translate(upper_washer_center_r) rotate([0,90,0]) cylinder(h=washer_thickness, r=hole_r+washer_wall, center=false); // right upper washer
				translate(upper_washer_center_l) rotate([0,90,0]) cylinder(h=washer_thickness, r=hole_r+washer_wall, center=false); // left upper washer
				translate(lower_washer_center_r) rotate([0,90,0]) cylinder(h=washer_thickness, r=hole_r+washer_wall, center=false); // right lower washer
				translate(lower_washer_center_l) rotate([0,90,0]) cylinder(h=washer_thickness, r=hole_r+washer_wall, center=false); // left lower washer
				translate(bracket_center) cube(bracket_outer, center=false); // outer bracket
			}
			translate(bracket_inner_center) cube(bracket_inner, center=false); // inner bracket
		}
		translate(upper_holes_center) rotate([0,90,0]) cylinder(h=holes_width, r=hole_r, center=false);
		translate(lower_holes_center) rotate([0,90,0]) cylinder(h=holes_width, r=hole_r, center=false);
	}
}

//
// This is one side of the Quick Attach bracket that will go inside the bucket bracket.
//
module qa_inner_wall() {
	depth = 2.5 * in + overlap;
	
	overall = [wall, depth, inner_wall_height];

	upper_hole_x = -fudge; // just to the left of the outside of the plate
	upper_hole_y = -.5 * in; // distance from back of plate
	upper_hole_z = (inner_wall_height/2) ;
	upper_hole_center = [upper_hole_x, upper_hole_y, upper_hole_z];

	lower_hole_x = upper_hole_x; // just to the left of the outside of the plate
	lower_hole_y = upper_hole_y; // distance from back of bucket
	lower_hole_z = upper_hole_z - hole_to_hole;
	lower_hole_center = [lower_hole_x, lower_hole_y, lower_hole_z];

	difference() {
		difference() {
			cube(overall, center=true);
			translate(upper_hole_center) rotate([0,90,0]) cylinder(h=wall + 3 * in, r=hole_r+dom_wall, center=true);
			translate(lower_hole_center) rotate([0,90,0]) cylinder(h=wall + 3 * in, r=hole_r+dom_wall, center=true);
		}
	}
}

module qa_inner_spacer() {
	difference() {
		rotate([0,90,0]) cylinder(h=width-wall*2, r=hole_r+dom_wall, center=true);
		translate([0, 0, 0]) rotate([0,90,0]) cylinder(h=width-wall*2+fudge+fudge, r=hole_r, center=true);
	}
}


module qa_inner_bracket() {
	// First render both walls
	color("DarkGreen") translate([org(width-wall)+wall,overlap,0]) qa_inner_wall();	// right inner
	color("DarkGreen") translate([-(org(width-wall)+wall),overlap,0]) qa_inner_wall(); // left inner

	upper_spacer_x = -fudge;
	upper_spacer_y = (-.5 * in); // distance from back of bucket
	upper_spacer_z = (inner_wall_height / 2);
	upper_spacer_center = [upper_spacer_x, upper_spacer_y+overlap, upper_spacer_z];

	lower_spacer_x = upper_spacer_x; // just to the left of the outside of the plate
	lower_spacer_y = upper_spacer_y; // distance from back of bucket
	lower_spacer_z = upper_spacer_z - hole_to_hole;
	lower_spacer_center = [lower_spacer_x, lower_spacer_y+overlap, lower_spacer_z];

	difference() {
		union() {
			difference() {
				color("Yellow") translate(upper_spacer_center) qa_inner_spacer();	// upper spacer
				translate([upper_spacer_x - 2.5 * in,upper_spacer_y - 2.5*in, upper_spacer_z ]) cube([5*in,5*in,5*in],center=false);
			}
			color("Yellow") translate([upper_spacer_x, upper_spacer_y-overlap-wall-.125*in, upper_spacer_z + .95*in]) rotate([20,0,0]) cube([width-wall*2,wall,2 * in], center=true);
		}
		translate([upper_spacer_x-fudge, upper_spacer_y-overlap-.025*in, upper_spacer_z + .8*in]) rotate([20,0,0]) cube([width,wall,2 * in], center=true);
	}
	color("Yellow") translate(lower_spacer_center) qa_inner_spacer();	// lower spacer
}

module qa_outer_wall() {
	depth = 3.0 * in;
	wall = .250 * in;
	
	overall = [wall, depth, outer_wall_height];

	upper_hole_x = -fudge; // just to the left of the outside of the plate
	upper_hole_y = .5 * in; // distance from back of plate
	upper_hole_z = (outer_wall_height/2) - .75 * in ;
	upper_hole_center = [upper_hole_x, upper_hole_y, upper_hole_z];

	lower_hole_x = upper_hole_x; // just to the left of the outside of the plate
	lower_hole_y = upper_hole_y; // distance from back of bucket
	lower_hole_z = upper_hole_z - hole_to_hole;
	lower_hole_center = [lower_hole_x, lower_hole_y, lower_hole_z];

	difference() {
		difference() {
			cube(overall, center=true);
			translate(upper_hole_center) rotate([0,90,0]) cylinder(h=wall + 5 * in, r=hole_r+washer_thickness, center=true);  // drill these out to OD DOM
			translate(lower_hole_center) rotate([0,90,0]) cylinder(h=wall + 5 * in, r=hole_r+washer_thickness, center=true);  // drill these out to OD DOM
		}
	}
}

//
// This is an assembly module only. It renders the two walls and then adds washers.
//
module qa_outer_bracket() {
	// First render the two walls
	color("DarkGreen") translate([-(org(width+wall)+wall),overlap,0]) qa_outer_wall(left=true); // left outer
	color("DarkGreen") translate([org(width+wall)+wall,overlap,0]) qa_outer_wall(left=false);	// right outer

	upper_hole_x = -(org(width)-wall); // just to the left of the outside of the plate
	upper_hole_y = .5 * in + overlap; // distance from back of plate
	upper_hole_z = (outer_wall_height/2) - .75 * in ;
	upper_hole_center = [upper_hole_x, upper_hole_y, upper_hole_z];

	upper_washer_center_r = [upper_hole_x-(2 * wall)+fudge, upper_hole_y, upper_hole_z];
	upper_washer_center_l = [-upper_hole_x-(wall)+(washer_thickness)+fudge, upper_hole_y, upper_hole_z];

	lower_hole_x = upper_hole_x; // just to the left of the outside of the plate
	lower_hole_y = upper_hole_y; // distance from back of bucket
	lower_hole_z = upper_hole_z - hole_to_hole;
	lower_hole_center = [lower_hole_x, lower_hole_y, lower_hole_z];

	lower_washer_center_r = [lower_hole_x-(2 * wall)+fudge, lower_hole_y, lower_hole_z];
	lower_washer_center_l = [-lower_hole_x-(wall)+(washer_thickness)+fudge, lower_hole_y, lower_hole_z];

	difference() {
		translate(upper_washer_center_l) rotate([0,90,0]) cylinder(h=2*washer_thickness, r=hole_r+washer_wall, center=false); // left upper washer
		translate(upper_washer_center_l) translate([-1,0,0]) rotate([0,90,0]) cylinder(h=4*washer_thickness, r=hole_r, center=false); // left upper washer
	}

	difference() {
		translate(upper_washer_center_r) rotate([0,90,0]) cylinder(h=2*washer_thickness, r=hole_r+washer_wall, center=false); // right upper washer
		translate(upper_washer_center_r) translate([-1,0,0]) rotate([0,90,0]) cylinder(h=4*washer_thickness, r=hole_r, center=false); // right upper washer
	}
	difference() {
		translate(lower_washer_center_l) rotate([0,90,0]) cylinder(h=2*washer_thickness, r=hole_r+washer_wall, center=false); // left lower washer
		translate(lower_washer_center_l) translate([-1,0,0]) rotate([0,90,0]) cylinder(h=4*washer_thickness, r=hole_r, center=false); // left lower washer
	}
	difference() {
		translate(lower_washer_center_r) rotate([0,90,0]) cylinder(h=2*washer_thickness, r=hole_r+washer_wall, center=false); // right lower washer
		translate(lower_washer_center_r) translate([-1,0,0]) rotate([0,90,0]) cylinder(h=4*washer_thickness, r=hole_r, center=false); // left lower washer
	}

}

//color("OrangeRed")  bucket_bracket();
//translate([0,overlap+.25*in,-.75*in]) qa_inner_bracket();
translate([0,(8*overlap+.25*in),-.75*in]) qa_outer_bracket();
